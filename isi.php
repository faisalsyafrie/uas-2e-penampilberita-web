<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Penampil Berita</title>
	 <style>
   td{
	   padding:10px;
   }
   th{
	   background-color:#01B0CF;
	   color:white;
	   padding:5px;

   }
   .edit{
	   padding:5px;
	   background-color:yellowgreen;
	   color:white;
	   text-decoration:none;
   }
   .edit:hover{
	   background-color:#729c1c;
	     color:white;
	   text-decoration:none;
   }
   .hapus{
	   padding:5px;
	   background-color:#fb6161;
	   color:white;
	   text-decoration:none;
   }
   .hapus:hover{
	   background-color:red;
	     color:white;
	   text-decoration:none;
   }
    .baru{
	   padding:5px;
	   background-color:#6c6cf5;
	   color:white;
	   text-decoration:none;
   }
   .baru:hover{
	   background-color:blue;
	     color:white;
	   text-decoration:none;
   }
  </style>
  </head>
  <body>
 
   
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">Penampil Berita</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
	  
    </ul>
  </div>
</nav>

<div class="jumbotron jumbotron-fluid bg-danger mb-5 " style="background-image:url(background2.jpg);">
  <div class="container text-center rounded-sm">
	
    <font face="Ultra Fresh - D">
	<h1 class="display-4 text-light font-weight-bolder " style="text-shadow:0 0 3px black, 0 0 5px black;">Penampil Berita</h1>
    </font>
	<p class="lead font-italic text-light font-weight-bolder" style="text-shadow:0 0 3px black, 0 0 5px black;">Berita terkini dan terbaru.</p>
  </div>
</div>

<span id="tbuku"></span>
<div class="container text-center mb-10">
<?php
require("koneksi.php");
$get=$_GET['isi'];
$query=mysqli_query($conn, "select * from detail WHERE judul LIKE '%$get%'");

while($data=mysqli_fetch_assoc($query)){
$query2=mysqli_query($conn, "select * from kategori where id_kategori='$data[id_kategori]'");
$query3=mysqli_fetch_assoc($query2);
?>
<h1 ><i><?php echo $data['judul'];?></i></h1>
<h5 class="text-primary"><?php echo $data['tanggal'];?></h5>
<h5 class="mb-5 text-warning"><?php echo $query3['nama_kategori'];?></h5>

<img src="images/<?php echo $data['photos'];?>"  class="shadow-lg p-2 mb-5 bg-white">
<h4 class="mb-5"><?php echo $data['isi'];?></h4> 
<?php
}
?>   
</div>




<footer class="bg-dark text-light text-while pt-3 mt-10">
<div class ="container">
<div class="row">
<div class="col text-center">
<p> Copyrights 2019</p>
</div>
</div>
</footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>